from django.apps import AppConfig


class StackoverflowlistConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'stackoverflowlist'
    verbose_name = 'Список вопросов'
