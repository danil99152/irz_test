from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views

app_name = 'stackoverflowlist'
router = routers.SimpleRouter()
router.register('', views.QuestionView, 'question')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('search/', views.SearchView.as_view())
]
