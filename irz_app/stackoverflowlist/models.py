from django.db import models


class Question(models.Model):
    title = models.CharField(max_length=100)
    creation_date = models.DateTimeField('date published')
    author = models.CharField(max_length=50)
    status = models.BooleanField()
    link = models.TextField()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'вопрос'
        verbose_name_plural = 'вопросы'

