import json
from datetime import datetime
from html import unescape

import pytz
import requests
from django.http import Http404
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Question
from .serializers import QuestionSerializer


def make_api_url(request):
    url = 'https://api.stackexchange.com/2.3/search?'
    try:
        for key, value in request.items():
            if value != "":
                url = url + f"&{key}={value}"
    finally:
        pass
    url = url + '&site=stackoverflow'
    return url


def saveQ(text):
    QuestionView.queryset.delete()
    for item in text['items']:
        q = Question(title=unescape(item['title']),
                     creation_date=datetime.fromtimestamp(item['creation_date'],
                                                          tz=pytz.UTC).isoformat(),
                     author=unescape(item['owner']['display_name']),
                     status=item['is_answered'],
                     link=item['link'])
        q.save()


class QuestionView(viewsets.ModelViewSet):
    serializer_class = QuestionSerializer
    queryset = Question.objects.all()
    queryset.delete()


class SearchView(APIView):
    @classmethod
    def post(cls, request):
        body_unicode = request.body.decode('utf-8')
        try:
            body_data = json.loads(body_unicode)
            print("Request:", body_data)
            if 'delete' in body_data:
                QuestionView.queryset.delete()
                return Response(body_data, status=status.HTTP_200_OK)
            elif 'deleteId' in body_data:
                QuestionView.queryset.get(id=body_data['deleteId']).delete()
                resp = json.dumps({'deleted': body_data['deleteId']})
                return Response(resp, status=status.HTTP_200_OK)
            elif "id" in body_data:
                user = QuestionView.queryset.get(pk=body_data['id'])
                resp = json.dumps({'id': user.id,
                                   'title': user.title,
                                   'creation_date': str(user.creation_date),
                                   'author': user.author,
                                   'status': user.status,
                                   'link': user.link}, indent=6)
                return Response(resp, status=status.HTTP_200_OK)
            else:
                url = requests.get(make_api_url(body_data))
                text = json.loads(url.text)
                if 'error_message' in text:
                    QuestionView.queryset.delete()
                    response = json.dumps({'error_message': text['error_message']})
                    print("Response:", response)
                    return Response(response, status=status.HTTP_200_OK)
                elif ('items' in text) and (not text['items']):
                    QuestionView.queryset.delete()
                    response = json.dumps({"error_message": "List is empty"})
                    print("Response:", response)
                    return Response(response, status=status.HTTP_200_OK)
                elif text['items']:
                    saveQ(text)
                    return Response(body_data, status=status.HTTP_200_OK)
        except Exception as e:
            raise Http404("Ошибка: ", e)
