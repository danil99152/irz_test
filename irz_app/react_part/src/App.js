import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {Button, Col, Form, FormGroup, Input, Label, ListGroup, ListGroupItem, Row} from "reactstrap";
import Select from 'react-select';
import Modal from 'react-modal';
import DatePicker from 'react-date-picker';

const url = "http://127.0.0.1:8000/api/";
const url_post = "http://127.0.0.1:8000/search/";

Modal.setAppElement('#root');

function App() {
  const [apiResponse, setApiResponse] = useState("");
  const [pageValue, setPageValue] = useState("");
  const [todateValue, setTodateValue] = useState("");
  const [maxValue, setMaxValue] = useState("");
  const [nottaggedValue, setNottaggedValue] = useState("");
  const [pagesizeValue, setPagesizeValue] = useState("");
  const [orderValue, setOrderValue] = useState("desc");
  const [sortValue, setSortValue] = useState("activity");
  const [intitleValue, setIntitleValue] = useState("");
  const [fromdateValue, setFromdateValue] = useState("");
  const [minValue, setMinValue] = useState("");
  const [taggedValue, setTaggedValue] = useState("");
  const [successCounter, setSuccessCounter] = useState(0);


  const callRestApi = async () => {
  const response = await fetch(url);
  const jsonResponse = await response.json();
  console.log(jsonResponse);
  if (jsonResponse.length !== 0){
    return jsonResponse.map(
        record => <ListGroupItem key={record.id}><a onClick={() => actionResponse(record.id)} value={record.id}>{record.title} written by {record.author}</a></ListGroupItem>
    );
  }
};
const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    height: "50%",
    width: "50%",
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};
const [user, setUser] = useState(JSON.parse(JSON.stringify({
      id: '',
      title: '',
      author: '',
      creation_date: '',
      status: '',
      link: ''
  })));

async function actionResponse(id){
    const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({"id": id})
  }
  const response = await fetch(url_post, options);
  const jsonResponse = await response.json();
  const user = JSON.parse(jsonResponse)
  console.log(user);
  setUser(afterOpenModal(user));
  openModal();
}

async function response(RecordBodyParameters) {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(RecordBodyParameters)
  }

  const response = await fetch(url_post, options);
  const jsonResponse = await response.json();
  console.log(JSON.stringify(JSON.stringify(RecordBodyParameters)));
  console.log(JSON.stringify(jsonResponse));
  if (typeof jsonResponse === "string") {
    var resp = JSON.parse(jsonResponse);
    console.log(resp.error_message);
    if (resp.error_message) {
      console.log(resp)
      return (<p>{resp}</p>)
    }
  } else {
    return jsonResponse
  }
}

const purge = async () => {
  const RecordBodyParameters = {
    'delete': 'True',
  }
  await response(RecordBodyParameters);
};

  useEffect(() => {
    callRestApi().then(
      result => setApiResponse(result));
  }, [successCounter]);

  function HandlePageChange(event) {
    setPageValue(event.target.value);
  }
  function HandleTodateChange(event) {
      if (event !== null) {
          const date = new Date(`${
          event.getFullYear()
        }/${event.getMonth() + 1}/${event.getDate()}`);
        setTodateValue(date)
      } else {
        setTodateValue(null)
      }
  }
  function HandleMaxChange(event) {
      if (event !== null) {
          const date = new Date(`${
              event.getFullYear()
          }/${event.getMonth() + 1}/${event.getDate()}`);
          setMaxValue(date);
      } else {
          setMaxValue(null)
      }
  }
  function HandleNottaggedChange(event) {
    setNottaggedValue(event.target.value);
  }
  function HandlePagesizeChange(event) {
    setPagesizeValue(event.target.value);
  }
  function HandleOrderChange(event) {
    setOrderValue(event.value);
  }
  function HandleSortChange(event) {
    setSortValue(event.value);
  }
  function HandleIntitleChange(event) {
    setIntitleValue(event.target.value);
  }
  function HandleFromdateChange(event) {
    if (event !== null) {
        const date = new Date(`${
            event.getFullYear()
        }/${event.getMonth() + 1}/${event.getDate()}`);
        setFromdateValue(date);
    } else{
        setFromdateValue(null)
    }
  }
  function HandleMinChange(event) {
      if (event !== null) {
          const date = new Date(`${
              event.getFullYear()
          }/${event.getMonth() + 1}/${event.getDate()}`);
          setMinValue(date);
      } else {
          setMinValue(null)
      }
  }
  function HandleTaggedChange(event) {
    setTaggedValue(event.target.value);
  }

  function validDate(date){
      if (date === ''){
          return ''
      } else {
          let time = date.getFullYear().toString()
          if (date.getMonth() < 9)
              time += '-0'+(date.getMonth()+1).toString()
          else
              time += '-'+(date.getMonth()+1).toString()
          if (date.getDate() < 10)
              time += '-0'+date.getDate().toString()
          else
              time += '-'+date.getDate().toString()
          return time
      }
  }

  function ButtonClick() {
      const RecordBodyParameters = {
      'page': pageValue,
      'todate': validDate(todateValue),
      'max' : validDate(maxValue),
      'nottagged': nottaggedValue,
      'pagesize': pagesizeValue,
      'order': orderValue,
      'sort': sortValue,
      'intitle': intitleValue,
      'fromdate': validDate(fromdateValue),
      'min': validDate(minValue),
      'tagged': taggedValue
    }
    setApiResponse(apiResponse);
      response(RecordBodyParameters).then(
      result => {
          if (typeof result.props !== "undefined"){
          setApiResponse(result.props.children.error_message);
      } else {
          setSuccessCounter(successCounter + 1);
      }
      })
  }
  function DeleteClick() {
    setApiResponse(apiResponse);
    purge().then(() => {
          setSuccessCounter(successCounter + 1);
        }
      );
  }

  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal(loc_user) {
    user.id = loc_user.id;
    user.title = loc_user.title;
    user.author = loc_user.author;
    user.creation_date = loc_user.creation_date;
    if (loc_user.status === true)
        user.status = "Still open";
    else
        user.status = "Closed";
    user.link = loc_user.link;
    return user
  }

  function closeModal() {
    setIsOpen(false);
  }

  const sort = [
      { value: 'activity', label: 'activity', id: 'sort-input' },
      { value: 'votes', label: 'votes', id: 'sort-input' },
      { value: 'creation', label: 'creation', id: 'sort-input' },
      { value: 'relevance', label: 'relevance', id: 'sort-input' }
  ];
  const order = [
      { value: 'desc', label: 'desc', id: 'order-input' },
      { value: 'asc', label: 'asc', id: 'order-input' }
  ];

    async function DeleteById(id) {
        const RecordBodyParameters = {
            'deleteId': id
        }
        await response(RecordBodyParameters);
        closeModal();
        setSuccessCounter(successCounter + 1);
    }

  return (
    <div>
      <h1>StackExchange</h1>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
      >
        <h1>{user.author}</h1>
          <Button color="danger" type="button" onClick={() =>DeleteById(user.id)}>Удалить</Button>
        <ListGroup>
          <b>Title</b>
          <ListGroupItem>{user.title}</ListGroupItem>
          <b>Link</b>
          <ListGroupItem>{user.link}</ListGroupItem>
            <b>Creation_date</b>
          <ListGroupItem>{user.creation_date}</ListGroupItem>
            <b>Status</b>
          <ListGroupItem>{user.status}</ListGroupItem>

        </ListGroup>
      </Modal>
        <Row>
            <Col sm={6}>
          <Form>
            <FormGroup>
              <Label sm={2} size={2}>page</Label>
              <Col sm={5}>
              <Input type="text" value={pageValue} id="page-input" onChange={HandlePageChange} />
              </Col>
            </FormGroup>

            <FormGroup>
              <Label sm={2} size={2}>todate</Label>
                <Col sm={5}>
                    <DatePicker format="y-MM-dd" onChange={HandleTodateChange} value={todateValue} />
                </Col>
            </FormGroup>

             <FormGroup>
              <Label sm={2} size={2}>max</Label>
                 <Col sm={5}>
                    <DatePicker format="y-MM-dd" onChange={HandleMaxChange} value={maxValue} />
              </Col>
            </FormGroup>

            <FormGroup>
             <Label sm={2} size={2}>nottagged</Label>
             <Col sm={5}>
              <Input type="text" value={nottaggedValue} id="nottagged-input" onChange={HandleNottaggedChange} />
             </Col>
            </FormGroup>

             <FormGroup>
              <Label sm={2} size={2}>pagesize</Label>
                <Col sm={5}>
              <Input type="text" value={pagesizeValue} id="pagesize-input" onChange={HandlePagesizeChange} />
                </Col>
            </FormGroup>

            <FormGroup>
              <Label sm={2} size={2}>order</Label>
                <Col sm={5}>
              <Select defaultValue={orderValue} onChange={HandleOrderChange} options={order}/>
                </Col>
            </FormGroup>

             <FormGroup>
              <Label sm={2} size={2}>sort</Label>
                 <Col sm={5}>
               <Select defaultValue={sortValue} onChange={HandleSortChange} options={sort}/>
                </Col>
            </FormGroup>

            {/*<div>*/}
            {/*  <label htmlFor="order-input">order</label>*/}
            {/*  <select value={orderValue} onChange={HandleOrderChange}>*/}
            {/*    <option value="desc" id="order-input" onChange={HandleOrderChange}>desc</option>*/}
            {/*    <option value="asc" id="order-input" onChange={HandleOrderChange}>asc</option>*/}
            {/*  </select>*/}
            {/*</div>*/}

            {/* <div>*/}
            {/*  <label htmlFor="sort-input">sort</label>*/}
            {/*   <select value={sortValue} onChange={HandleSortChange}>*/}
            {/*    <option value="activity" id="sort-input" onChange={HandleSortChange}>activity</option>*/}
            {/*    <option value="votes" id="sort-input" onChange={HandleSortChange}>votes</option>*/}
            {/*    <option value="creation" id="sort-input" onChange={HandleSortChange}>creation</option>*/}
            {/*    <option value="relevance" id="sort-input" onChange={HandleSortChange}>relevance</option>*/}

            {/*  </select>*/}
            {/*</div>*/}

            <FormGroup>
               <Label sm={2} size={2}>intitle</Label>
            <Col sm={5}>
            <Input type="text" value={intitleValue} id="intitle-input" onChange={HandleIntitleChange} />
            </Col>
            </FormGroup>

             <FormGroup>
               <Label sm={2} size={2}>fromdate</Label>
              <Col sm={5}>
                    <DatePicker format="y-MM-dd" onChange={HandleFromdateChange} value={fromdateValue} />
              </Col>
            </FormGroup>

            <FormGroup>
               <Label sm={2} size={2}>min</Label>
              <Col sm={5}>
                    <DatePicker format="y-MM-dd" onChange={HandleMinChange} value={minValue} />
              </Col>
            </FormGroup>

             <FormGroup>
               <Label sm={2} size={2}>tagged</Label>
              <Col sm={5}>
                 <Input type="text" value={taggedValue} id="tagged-input" onChange={HandleTaggedChange} />
              </Col>
              </FormGroup>

              <FormGroup>
                <Row>
                <Col sm={2}>
                    <Button color="primary" variant="contained" type="button" onClick={ButtonClick}>Начать поиск</Button>
                </Col>
                <Col>
                    <Button type="button" variant="contained" onClick={DeleteClick}>Очистить список</Button>
                </Col>
                </Row>
              </FormGroup>

          </Form>
            </Col>
          <Col sm={5}>
                <ListGroup>{apiResponse}</ListGroup>
          </Col>
      </Row>
    </div>
  );
}

ReactDOM.render(
  <App />,
  document.querySelector('#root')
);

export default App;